﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Models
{
    [DataContract(Namespace = "", Name = "Message")]
    public class Message
    {
        [DataMember(Name = "SenderGLKNumber")]
        public int SenderGLKNumber;

        [DataMember(Name = "ReceiverGLKNumber")]
        public int ReceiverGLKNumber;

        [DataMember(Name = "MessageCnt")]
        public string MessageContent;

        [DataMember(Name = "SendDate")]
        public DateTime SendDate;
    }
}