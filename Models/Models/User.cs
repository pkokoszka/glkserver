﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Models
{

    [DataContract(Namespace = "", Name = "User")]
    public class User
    {
        [DataMember(Name = "GLKNumber")]
        public int GLKNumber;

        [DataMember(Name = "Password")]
        public string Password;

        [DataMember(Name = "UserIP")]
        public string UserIP;

        [DataMember(Name = "Firstname")]
        public string Firstname;

        [DataMember(Name = "Surname")]
        public string Surname;

        [DataMember(Name = "Email")]
        public string Email;

        [DataMember(Name = "BirthDay")]
        public string BirthDay;
    }
}