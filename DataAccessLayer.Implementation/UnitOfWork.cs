﻿using DataAccessLayer.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        //private DbContext context;
        private DbContext dbContext;
        private DbContext DbContext
        {
            get { return dbContext ?? (dbContext = new DataAccessLayer.Implementation.glkcommunicatorEntities()); }
        }

        public UnitOfWork()
        {
            
        }

        internal DbSet<T> GetDbSet<T>()
            where T : class
        {
            return DbContext.Set<T>();
        }

        public void Commit()
        {

            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(
                    string.Format("There was an error with database: {0}, {1}, {2}, {3}, {4}.", ex.Message, ex.Data, ex.InnerException, ex.HResult, ex.StackTrace),
                    HttpStatusCode.InternalServerError);
            }
            
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}
