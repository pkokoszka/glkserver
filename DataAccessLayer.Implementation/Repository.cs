﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.Contracts;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DataAccessLayer.Implementation
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private readonly DbSet<T> dbSet;

        public Repository(IUnitOfWork unitOfWork)
        {
            var efUnitOfWork = unitOfWork as UnitOfWork;
            if (efUnitOfWork == null) throw new Exception("Must be UnitOfWork"); // TODO: Typed exception
            dbSet = efUnitOfWork.GetDbSet<T>();
        }




        public void Add(T item)
        {
            dbSet.Add(item);
        }

        public void Remove(T item)
        {
            dbSet.Remove(item);
        }

        public IQueryable<T> Query()
        {
            return dbSet;
        }
    }
}
