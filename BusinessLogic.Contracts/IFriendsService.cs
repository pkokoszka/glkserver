﻿using System;
namespace BusinessLogic.Contracts
{
    public interface IFriendsService
    {
        BusinessLogic.Contracts.IAccountService accountService { get; set; }
        Models.Account AddFriend(int userGLKNumber, int friendGLKNumber);
        bool DeleteFriend(int userGLKNumber, int friendGLKNumber);
        Models.Accounts GetFriendsList(int userGLKNumber);
        bool GetFriendStatus(int friendGLKNumber);
        DataAccessLayer.Contracts.IRepository<DataAccessLayer.Implementation.Entites.SessionLoggingEntity> sessionLogginRepository { get; set; }
        DataAccessLayer.Contracts.IUnitOfWork unitOfWork { get; set; }
        DataAccessLayer.Contracts.IRepository<DataAccessLayer.Implementation.Entites.UserFriendEntity> userFriendRepository { get; set; }
        DataAccessLayer.Contracts.IRepository<DataAccessLayer.Implementation.Entites.UserEntity> userRepository { get; set; }
    }
}
