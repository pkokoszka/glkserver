﻿using System;
namespace BusinessLogic.Contracts
{
    public interface ILogginService
    {
        BusinessLogic.Contracts.IAccountService accountService { get; set; }
        void AddSession(int UserGLKNumber, string SessionToken, string IP);
        bool ChangeStatus(string SessionToken, bool IsOnline);
        string CreateMD5Hash(string input);
        bool ExtendSessionToken(string SessionToken);
        string GenerateSessionToken(int GLKNumber);
        DataAccessLayer.Implementation.Entites.SessionLoggingEntity getSessionLoggin(string SessionToken);
        DataAccessLayer.Implementation.Entites.SessionLoggingEntity getSessionLoggin(string SessionToken, int GLKNumber, bool UnLogged, DateTime LastConnection);
        bool SessionLogginIsNull(string SessionToken);
        DataAccessLayer.Contracts.IRepository<DataAccessLayer.Implementation.Entites.SessionLoggingEntity> sessionLogginRepository { get; set; }
        DataAccessLayer.Contracts.IUnitOfWork unitOfWork { get; set; }
        bool UnlogUser(string SessionToken, int GLKNumber, bool UnLogged);
        DataAccessLayer.Contracts.IRepository<DataAccessLayer.Implementation.Entites.UserEntity> userRepository { get; set; }
    }
}
