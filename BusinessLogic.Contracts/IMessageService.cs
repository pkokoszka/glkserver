﻿using System;
namespace BusinessLogic.Contract
{
    public interface IMessageService
    {
        BusinessLogic.Contracts.IAccountService accountService { get; set; }
        Models.Messages GetMessages(string SessionToken, int UserGLKNumber);

        Models.Messages GetMessagesFromUser(string SessionToken, int UserGLKNumber, int FriendGLKNumber);
        DataAccessLayer.Contracts.IRepository<DataAccessLayer.Implementation.Entites.MessageEntity> messageRepository { get; set; }
        Models.Message SendMessage(Models.Message message);
        DataAccessLayer.Contracts.IUnitOfWork unitOfWork { get; set; }
    }
}
