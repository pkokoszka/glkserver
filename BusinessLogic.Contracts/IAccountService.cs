﻿using DataAccessLayer.Contracts;
using DataAccessLayer.Implementation.Entites;
using Models;
using System;
namespace BusinessLogic.Contracts
{
    public interface IAccountService
    {
        bool CheckAuthorization(int GLKNumber, string Password);
        Account RegisterUser(User user);
        bool CheckAuthorization(int GLKNumber, string Password, string SessionToken);
        UserEntity getUserEntity(int GLKNumber);
        IRepository<DataAccessLayer.Implementation.Entites.SessionLoggingEntity> sessionLogginRepository { get; set; }
        IUnitOfWork UnitOfWork { get; set; }
        IRepository<DataAccessLayer.Implementation.Entites.UserEntity> userRepository { get; set; }
    }
}
