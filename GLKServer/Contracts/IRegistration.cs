﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;



namespace GLKServer.Contracts
{
    [ServiceContract]
    public interface IRegistration
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/RegisterUser", Method = "POST")]
        Account RegisterUser(User user);
    }

}
