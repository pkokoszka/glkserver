﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GLKServer.Contracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMessenger" in both code and config file together.
    [ServiceContract]
    public interface IMessenger
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/SendMessage/{SessionToken}/{UserPassword}/{UserGLKNumber}", Method = "POST")]
        Message SendMessage(Message message, string SessionToken, string UserPassword, string UserGLKNumber);


        [OperationContract]
        [WebGet(UriTemplate = "/GetMessages/{SessionToken}/{UserPassword}/{UserGLKNumber}")]
        Messages GetAllMessages(string SessionToken, string UserPassword, string UserGLKNumber);

        [OperationContract]
        [WebGet(UriTemplate = "/GetMessages/{SessionToken}/{OwnerPassword}/{OwnerGLKNumber}/{FriendGLKNumber}")]
        Messages GetMessagesFromUser(string SessionToken, string OwnerPassword, string OwnerGLKNumber, string FriendGLKNumber);
    }
}
