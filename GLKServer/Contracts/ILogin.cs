﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GLKServer.Contracts
{
    [ServiceContract]
    public interface ILogin
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/LoginUser", Method = "POST")]
        string LoginUser(User user);


        [OperationContract]
        [WebGet(UriTemplate = "/GetUser/{SessionToken}")]
        Account GetUser(string SessionToken);

        [OperationContract]
        [WebGet(UriTemplate = "/ExtendSession/{SessionToken}")]
        bool ExtendSession(string SessionToken);

        [OperationContract]
        [WebGet(UriTemplate = "/ExtendSession/{SessionToken}/{isOnline}")]
        bool ChangeStatus(string SessionToken, string isOnline);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UnlogUser/{SessionToken}", Method = "POST")]
        bool UnlogUser(User user, string SessionToken);

    }
}
