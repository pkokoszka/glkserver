﻿
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GLKServer.Contracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFriends" in both code and config file together.
    [ServiceContract]
    public interface IFriends
    {

        [OperationContract]
        [WebGet(UriTemplate = "/AddFriend/{UserPassword}/{UserGLKNumber}/{FriendGLKNumber}")]
        Account AddFriend(string UserPassword, string UserGLKNumber, string FriendGLKNumber);

        [OperationContract]
        [WebGet(UriTemplate = "/DeleteFriend/{UserPassword}/{UserGLKNumber}/{FriendGLKNumber}")]
        bool DeleteFriend(string UserPassword, string UserGLKNumber, string FriendGLKNumber);

        [OperationContract]
        [WebGet(UriTemplate = "/FriendStatus/{UserPassword}/{UserGLKNumber}/{FriendGLKNumber}")]
        string GetFriendStatus(string UserPassword, string UserGLKNumber, string FriendGLKNumber);

        [OperationContract]
        [WebGet(UriTemplate = "/FriendsList/{UserPassword}/{UserGLKNumber}")]
        Accounts GetFriendsList(string UserPassword, string UserGLKNumber);
    }
}
