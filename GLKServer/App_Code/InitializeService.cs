﻿using DataAccessLayer.Implementation.Entites;
using DataAccessLayer.Implementation;
using GLKServer.Contracts;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AutoMapper;
using Ninject.Web.Common;

namespace GLKServer
{
    
    public class InitializeService
    {
        
        public static void AppInitialize()
        {
            
            MapperConfig.RegisterMaps();



        }
    }

    public static class MapperConfig
    {
        public static void RegisterMaps()
        {

            Mapper.CreateMap<MessageEntity, Message>()
                .ForMember(dest => dest.MessageContent, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.ReceiverGLKNumber, opt => opt.MapFrom(src => src.ReceiverGLKNumber))
                .ForMember(dest => dest.SenderGLKNumber, opt => opt.MapFrom(src => src.SenderGLKNumber))
                .ForMember(dest => dest.SendDate, opt => opt.MapFrom(src => src.SendDate));

            Mapper.CreateMap<UserEntity, Account>()
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Firstname, opt => opt.MapFrom(src => src.Firstname))
                .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.Surname))
                .ForMember(dest => dest.GLKNumber, opt => opt.MapFrom(src => src.GLKNumber));

            Mapper.CreateMap<UserFriendEntity, Account>()
                .ForMember(dest => dest.GLKNumber, opt => opt.MapFrom(src => src.FriendGLKNumber));

        }

    }


}