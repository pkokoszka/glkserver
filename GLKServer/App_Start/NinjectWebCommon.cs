[assembly: WebActivator.PreApplicationStartMethod(typeof(GLKServer.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(GLKServer.App_Start.NinjectWebCommon), "Stop")]

namespace GLKServer.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using BusinessLogic.Contracts;
    using DataAccessLayer.Contracts;
    using DataAccessLayer.Implementation.Entites;
    using BusinessLogic.Implementation;
    using DataAccessLayer.Implementation;
    using GLKServer.Services;
    using GLKServer.Contracts;
    using BusinessLogic.Contract;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();




            //kernel.Bind<Login>().ToSelf().InRequestScope();
            //kernel.Bind<Friends>().ToSelf().InRequestScope();
            //kernel.Bind<Messenger>().ToSelf().InRequestScope();
            //kernel.Bind<Registration>().ToSelf().InRequestScope();


            kernel.Bind<IAccountService>().To<AccountService>();
            kernel.Bind<ILogginService>().To<LogginService>();
            kernel.Bind<IMessageService>().To<MessageService>();
            kernel.Bind<IFriendsService>().To<FriendsService>();

            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope();

            kernel.Bind<IRepository<MessageEntity>>().To<Repository<MessageEntity>>();
            kernel.Bind<IRepository<SessionLoggingEntity>>().To<Repository<SessionLoggingEntity>>();
            kernel.Bind<IRepository<UserFriendEntity>>().To<Repository<UserFriendEntity>>();
            kernel.Bind<IRepository<UserEntity>>().To<Repository<UserEntity>>();

            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
        }        
    }

}
