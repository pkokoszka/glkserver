﻿using BusinessLogic.Contracts;
using GLKServer.Contracts;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GLKServer.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Friends" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Friends.svc or Friends.svc.cs at the Solution Explorer and start debugging.
    public class Friends : IFriends
    {

        public IAccountService accountService { get; set; }
        public IFriendsService friendsService { get; set; }

        public Friends(IAccountService _acc, IFriendsService _friends)
        {
            this.accountService = _acc;
            this.friendsService = _friends;
        }

        public Account AddFriend(string UserPassword, string UserGLKNumber, string FriendGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);
            int friendGLKNumber = Convert.ToInt32(FriendGLKNumber);


            if (UserPassword == "" || UserGLKNumber == "" || FriendGLKNumber == "")
                throw new WebFaultException<string>(
                    string.Format("GLKNumber cannot be null or Password cannot be null."),
                    HttpStatusCode.BadRequest);

            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);
            
            if (accountService.getUserEntity(friendGLKNumber) == null)
                throw new WebFaultException<string>(
                    string.Format("Cannot find user: {0}.", friendGLKNumber),
                    HttpStatusCode.BadRequest);

            var Account = friendsService.AddFriend(userGLKNumber, friendGLKNumber);
            return Account;
        }

        public bool DeleteFriend(string UserPassword, string UserGLKNumber, string FriendGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);
            int friendGLKNumber = Convert.ToInt32(FriendGLKNumber);


            if (UserPassword == "" || UserGLKNumber == "" || FriendGLKNumber == "")
                throw new WebFaultException<string>(
                    string.Format("GLKNumber cannot be null or Password cannot be null."),
                    HttpStatusCode.BadRequest);

            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);

            if (accountService.getUserEntity(friendGLKNumber) == null)
                throw new WebFaultException<string>(
                    string.Format("Cannot find user: {0}.", friendGLKNumber),
                    HttpStatusCode.BadRequest);

            return friendsService.DeleteFriend(userGLKNumber, friendGLKNumber);
        }

        public string GetFriendStatus(string UserPassword, string UserGLKNumber, string FriendGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);
            int friendGLKNumber = Convert.ToInt32(FriendGLKNumber);


            if (UserPassword == "" || UserGLKNumber == "" || FriendGLKNumber == "")
                throw new WebFaultException<string>(
                    string.Format("GLKNumber cannot be null or Password cannot be null."),
                    HttpStatusCode.BadRequest);

            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);

            if (accountService.getUserEntity(friendGLKNumber) == null)
                throw new WebFaultException<string>(
                    string.Format("Cannot find user: {0}.", friendGLKNumber),
                    HttpStatusCode.BadRequest);

            return friendsService.GetFriendStatus(friendGLKNumber).ToString();
        }

        public Accounts GetFriendsList(string UserPassword, string UserGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);


            if (UserPassword == "" || UserGLKNumber == "")
                throw new WebFaultException<string>(
                    string.Format("GLKNumber cannot be null or Password cannot be null."),
                    HttpStatusCode.BadRequest);

            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);

            var friendsList = friendsService.GetFriendsList(userGLKNumber);
            return friendsList;
        }
    }
}
