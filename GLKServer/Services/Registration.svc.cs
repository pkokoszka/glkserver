﻿using DataAccessLayer.Implementation.Entites;
using DataAccessLayer.Implementation;
using GLKServer.Contracts;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BusinessLogic.Contracts;

namespace GLKServer.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IRegistrationService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IRegistrationService.svc or IRegistrationService.svc.cs at the Solution Explorer and start debugging.
    public class Registration : IRegistration
    {
        public IAccountService accountService;


        public Registration(IAccountService _accountService, ILogginService _login)
        {
            this.accountService = _accountService;
        }

        
        
        public Account RegisterUser(User user)
        {
            if (user == null || user.Password == null || user.Password == "" || user.Firstname == null || user.Firstname == "")
            {
                throw new WebFaultException<string>(
                    string.Format("Password cannot be null or Firstname cannot be null."),
                    HttpStatusCode.Forbidden);
            }

            var accToReturn = accountService.RegisterUser(user);

            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = HttpStatusCode.OK;
            response.StatusDescription = "Account was created succesfully.";


            return accToReturn;
        }
    }
}
