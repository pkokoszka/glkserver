﻿using BusinessLogic;
using BusinessLogic.Contracts;
using DataAccessLayer.Implementation;
using DataAccessLayer.Implementation.Entites;
using GLKServer.Contracts;
using Models;
using System;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;

namespace GLKServer.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Login" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Login.svc or Login.svc.cs at the Solution Explorer and start debugging.
    public class Login : ILogin
    {
      

        public IAccountService accountService;
        public ILogginService logginService;

        public Login(IAccountService _accountService, ILogginService _login)
        {
            this.logginService = _login;
            this.accountService = _accountService;
        }

        public string LoginUser(User user)
        {
            if (user == null)
            {
                throw new WebFaultException<string>(
                    string.Format("GLK User cannot be null."),
                    HttpStatusCode.Forbidden);
            }

            if (user.Password == null || user.Password == "" || user.GLKNumber == 0)
            {
                throw new WebFaultException<string>(
                    string.Format("GLKNumber cannot be null or Firstname cannot be null."),
                    HttpStatusCode.Forbidden);
            }

            int UserGLKNumber = Convert.ToInt32(user.GLKNumber);

            if (accountService.CheckAuthorization(UserGLKNumber, user.Password))
            {
                string SessionToken = logginService.GenerateSessionToken(UserGLKNumber);


                logginService.AddSession(UserGLKNumber, SessionToken, user.UserIP);

                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = HttpStatusCode.OK;
                response.StatusDescription = "Logged successfully.";

                return SessionToken;
            }
            else
                throw new WebFaultException<string>(
                    string.Format("Authorization falied"),
                    HttpStatusCode.Forbidden);
        }

        public bool ExtendSession(string SessionToken)
        {

            if (SessionToken == null || SessionToken == "")
                throw new WebFaultException<string>(
                    string.Format("SessionToken cannot be null."),
                    HttpStatusCode.Forbidden);
            

            if (logginService.SessionLogginIsNull(SessionToken))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);
            else
            {
                if (logginService.ExtendSessionToken(SessionToken))
                {
                    OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    response.StatusCode = HttpStatusCode.OK;
                    response.StatusDescription = "Session was extended successfully.";

                    return true;
                }
                else
                {
                    throw new WebFaultException<string>(
                        string.Format("SessionToken expired. Please, login again."),
                        HttpStatusCode.RequestTimeout);
                }
            }
        }

        public bool UnlogUser(User user, string SessionToken)
        {
            if (accountService.CheckAuthorization(user.GLKNumber, user.Password))
            {
                if (logginService.UnlogUser(SessionToken, user.GLKNumber, false))
                {
                    OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    response.StatusCode = HttpStatusCode.OK;
                    response.StatusDescription = "User was unlogged successfully.";

                    return true;
                }
                else
                    throw new WebFaultException<string>(
                        string.Format("SessionToken expired or token was invalid."),
                        HttpStatusCode.RequestTimeout);

            }
            else
                 throw new WebFaultException<string>(
                        string.Format("Authorization falied."),
                        HttpStatusCode.Forbidden);
        }

        public bool ChangeStatus(string SessionToken, string isOnline)
        {
            bool IsOnline = Convert.ToBoolean(isOnline);

            if (SessionToken == null || SessionToken == "")
            {
                throw new WebFaultException<string>(
                    string.Format("SessionToken cannot be null."),
                    HttpStatusCode.Forbidden);
            }

            if (logginService.SessionLogginIsNull(SessionToken))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);
            else
            {
                if (logginService.ChangeStatus(SessionToken, IsOnline))
                {
                    OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    response.StatusCode = HttpStatusCode.OK;
                    response.StatusDescription = "Status was changed successfully.";

                    return IsOnline;
                }
                else
                {
                    throw new WebFaultException<string>(
                        string.Format("SessionToken expired. Please, login again."),
                        HttpStatusCode.RequestTimeout);
                }
            }
        }


        public Account GetUser(string SessionToken)
        {
            if (logginService.SessionLogginIsNull(SessionToken))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);

            var entity = logginService.getSessionLoggin(SessionToken);
            var user = accountService.getUserEntity(entity.GLKNumber);

            Account toReturn = new Account
            {
                BirthDay = user.BirthDay.ToString(),
                Email = user.Email,
                Firstname = user.Firstname,
                Surname = user.Surname,
                GLKNumber = user.GLKNumber,
                UserIP = entity.UserIP
            };

            return toReturn;
        }
    }
}
