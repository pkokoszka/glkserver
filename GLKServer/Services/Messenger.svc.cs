﻿using AutoMapper;
using BusinessLogic.Contract;
using BusinessLogic.Contracts;
using BusinessLogic.Implementation;
using DataAccessLayer.Implementation;
using DataAccessLayer.Implementation.Entites;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GLKServer.Contracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Messenger" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Messenger.svc or Messenger.svc.cs at the Solution Explorer and start debugging.
    public class Messenger : IMessenger
    {

        public IAccountService accountService { get; set; }
        public IMessageService messageService { get; set; }

        public Messenger(IAccountService _accountService, IMessageService _message)
        {
            this.messageService = _message;
            this.accountService = _accountService;
        }


        public Message SendMessage(Message message, string SessionToken, string UserPassword, string UserGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);

            if (message == null || message.ReceiverGLKNumber == 0 || userGLKNumber != message.SenderGLKNumber || message.SenderGLKNumber == 0 || message.MessageContent == null)
                throw new WebFaultException<string>(
                    string.Format("Message cannot be null or SenderGLKNumber cannot be null or ReceiverGLKNumber cannot be null."),
                    HttpStatusCode.BadRequest);

            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword, SessionToken))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);


            UserEntity sender = accountService.getUserEntity(message.SenderGLKNumber);
            UserEntity receiver = accountService.getUserEntity(message.ReceiverGLKNumber);

            if (sender == null || receiver == null)
                throw new WebFaultException<string>(
                    string.Format("Message cannot be null or SenderGLKNumber cannot be null or ReceiverGLKNumber cannot be null."),
                    HttpStatusCode.BadRequest);


            var messageToReturn = messageService.SendMessage(message);


            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = HttpStatusCode.OK;
            response.StatusDescription = "Sent successfully.";


            return messageToReturn;
        }

        public Messages GetAllMessages(string SessionToken, string UserPassword, string UserGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);

            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword, SessionToken))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);

            return messageService.GetMessages(SessionToken, userGLKNumber);
        }




        public Messages GetMessagesFromUser(string SessionToken, string UserPassword, string UserGLKNumber, string FriendGLKNumber)
        {
            int userGLKNumber = Convert.ToInt32(UserGLKNumber);
            int friendGLKNumber = Convert.ToInt32(FriendGLKNumber);


            if (!accountService.CheckAuthorization(userGLKNumber, UserPassword, SessionToken))
                throw new WebFaultException<string>(
                    string.Format("Authorization falied."),
                    HttpStatusCode.Forbidden);

            return messageService.GetMessagesFromUser(SessionToken, userGLKNumber, friendGLKNumber);
        }
    }
}
