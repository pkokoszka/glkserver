﻿using BusinessLogic.Contracts;
using DataAccessLayer.Contracts;
using DataAccessLayer.Implementation.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Implementation
{
    public class LogginService : ILogginService
    {
        public IAccountService accountService { get; set; }
        public IRepository<UserEntity> userRepository { get; set; }
        public IRepository<SessionLoggingEntity> sessionLogginRepository { get; set; }

        public IUnitOfWork unitOfWork { get; set; }

        public LogginService(IRepository<UserEntity> _user, IRepository<SessionLoggingEntity> _sessionRepository, IRepository<SessionLoggingEntity> _session, IUnitOfWork _unit, IAccountService _account)
        {
            accountService = _account;
            userRepository = _user;
            sessionLogginRepository = _session;
            unitOfWork = _unit;
        }

        public void AddSession(int UserGLKNumber, string SessionToken, string IP)
        {

            SessionLoggingEntity slEntity = new SessionLoggingEntity
            {
                FirstLogin = DateTime.Now,
                LastConnection = DateTime.Now,
                GLKNumber = UserGLKNumber,
                SessionToken = SessionToken,
                UserIP = IP,
                isOnline = true,
                Unlogged = false
            };


            sessionLogginRepository.Add(slEntity);
            unitOfWork.Commit();
        }

        public string GenerateSessionToken(int GLKNumber)
        {
            string Token = CreateMD5Hash(GLKNumber.ToString() + DateTime.Now);
            return Token;
        }

        public string CreateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                //sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();
        }

        public bool SessionLogginIsNull(string SessionToken)
        {
            if (getSessionLoggin(SessionToken) == null)
                return true;
            else
                return false;
        }

        public SessionLoggingEntity getSessionLoggin(string SessionToken)
        {
            return sessionLogginRepository.Query().SingleOrDefault(x => x.SessionToken == SessionToken);
        }



        public SessionLoggingEntity getSessionLoggin(string SessionToken, int GLKNumber, bool UnLogged, DateTime LastConnection)
        {
            return sessionLogginRepository.Query().FirstOrDefault(x => x.SessionToken == SessionToken
                && x.GLKNumber == GLKNumber
                && x.Unlogged == UnLogged
                && x.LastConnection > LastConnection);
        }

        public bool ExtendSessionToken(string SessionToken)
        {
            SessionLoggingEntity sle = getSessionLoggin(SessionToken);
            if (sle.LastConnection.Value.AddMinutes(1) >= DateTime.Now)
            {
                sle.LastConnection = DateTime.Now;
                unitOfWork.Commit();

                return true;
            }
            else
            {
                sle.Unlogged = true;
                unitOfWork.Commit();

                return false;
            }
        }

        public bool UnlogUser(string SessionToken, int GLKNumber, bool UnLogged)
        {
            DateTime toCheck = DateTime.Now.AddMinutes(-1);

            UserEntity userToUnlog = accountService.getUserEntity(GLKNumber);
            SessionLoggingEntity sle = getSessionLoggin(SessionToken, GLKNumber, UnLogged, toCheck);
            if (sle != null)
            {
                sle.Unlogged = true;
                unitOfWork.Commit();

                return true;
            }
            else
                return false;
        }

        public bool ChangeStatus(string SessionToken, bool IsOnline)
        {
            SessionLoggingEntity sle = getSessionLoggin(SessionToken);

            if (sle.LastConnection.Value.AddMinutes(1) >= DateTime.Now)
            {
                sle.isOnline = IsOnline;
                unitOfWork.Commit();

                return true;
            }
            else
            {
                sle.Unlogged = true;
                unitOfWork.Commit();

                return false;
            }
        }

    }
}
