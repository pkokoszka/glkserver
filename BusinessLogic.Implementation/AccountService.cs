﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Contracts;
using DataAccessLayer.Implementation.Entites;
using System.Security.Cryptography;
using BusinessLogic.Contracts;
using Models;

namespace BusinessLogic.Implementation
{

    
    public class AccountService : IAccountService
    {
        
        public IRepository<UserEntity> userRepository { get; set; }
        public IRepository<SessionLoggingEntity> sessionLogginRepository { get; set; }

        public IUnitOfWork UnitOfWork { get; set; }

        public AccountService(IRepository<UserEntity> _user, IRepository<SessionLoggingEntity> _session, IUnitOfWork _unit)
        {
            userRepository = _user;
            sessionLogginRepository = _session;
            UnitOfWork = _unit;
        }

        public bool CheckAuthorization(int GLKNumber, string Password)
        {
            UserEntity userToCheck = userRepository.Query().SingleOrDefault(x => x.GLKNumber == GLKNumber && x.Password == Password);

            if (userToCheck == null)
                return false;
            else
                return true;
        }


        public bool CheckAuthorization(int GLKNumber, string Password, string SessionToken)
        {
            UserEntity userToCheck = userRepository.Query().SingleOrDefault(x => x.GLKNumber == GLKNumber && x.Password == Password);
            SessionLoggingEntity sslEntity = sessionLogginRepository.Query().SingleOrDefault(x => x.SessionToken == SessionToken);


            if (userToCheck == null || sslEntity == null || sslEntity.GLKNumber != userToCheck.GLKNumber || sslEntity.LastConnection < DateTime.Now.AddMinutes(-1))
                return false;
            else
                return true;
        }

        public UserEntity getUserEntity(int GLKNumber)
        {
            return userRepository.Query().FirstOrDefault(x => x.GLKNumber == GLKNumber);
        }

        public Account RegisterUser(User user)
        {
            UserEntity entityToAdd = new UserEntity
            {
                Firstname = user.Firstname,
                Surname = user.Surname,
                Email = user.Email,
                del = false,
                Password = user.Password
            };


            int numberToAdd;
            do
            {
                Random random = new Random();
                numberToAdd = random.Next(1000, 9999);
            }
            while
                (userRepository.Query().Where(x => x.GLKNumber == numberToAdd).Count() > 0);

            entityToAdd.GLKNumber = numberToAdd;

            userRepository.Add(entityToAdd);
            UnitOfWork.Commit();

            Account accToReturn = new Account
            {
                Email = entityToAdd.Email,
                Firstname = entityToAdd.Firstname,
                Surname = entityToAdd.Surname,
                GLKNumber = entityToAdd.GLKNumber
            };


            return accToReturn;
        }




    }
}
