﻿using AutoMapper;
using BusinessLogic.Contracts;
using DataAccessLayer.Contracts;
using DataAccessLayer.Implementation.Entites;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Implementation
{
    public class FriendsService : IFriendsService
    {
        public IAccountService accountService { get; set; }
        public IRepository<UserEntity> userRepository { get; set; }
        public IRepository<UserFriendEntity> userFriendRepository { get; set; }
        public IRepository<SessionLoggingEntity> sessionLogginRepository { get; set; }

        public IUnitOfWork unitOfWork { get; set; }

        public FriendsService(IRepository<UserEntity> _user, IRepository<UserFriendEntity> _friend, IRepository<SessionLoggingEntity> _sessionRepository, IRepository<SessionLoggingEntity> _session, IUnitOfWork _unit, IAccountService _account)
        {
            accountService = _account;
            userRepository = _user;
            userFriendRepository = _friend;
            sessionLogginRepository = _session;
            unitOfWork = _unit;
        }

        public Account AddFriend(int userGLKNumber, int friendGLKNumber)
        {
            Account toReturn = null;
            if (userFriendRepository.Query().SingleOrDefault(x => x.FriendGLKNumber == friendGLKNumber && x.OwnerGLKNumber == userGLKNumber && x.del != true) == null)
            {
                UserFriendEntity toAdd = new UserFriendEntity
                {
                    del = false,
                    FriendGLKNumber = friendGLKNumber,
                    OwnerGLKNumber = userGLKNumber
                };

                userFriendRepository.Add(toAdd);
                unitOfWork.Commit();

                toReturn = Mapper.Map<UserEntity, Account>(accountService.getUserEntity(friendGLKNumber));
            }
            return toReturn;
        }

        public bool DeleteFriend(int userGLKNumber, int friendGLKNumber)
        {
            
            var toDelete = userFriendRepository.Query().SingleOrDefault(x => x.OwnerGLKNumber == userGLKNumber && x.FriendGLKNumber == friendGLKNumber && x.del != true);
            if (toDelete != null)
            {
                toDelete.del = true;
                unitOfWork.Commit();

                return true;
            }
            return false;
        }

        public bool GetFriendStatus(int friendGLKNumber)
        {
            var toCheck = DateTime.Now.AddMinutes(-1);
            SessionLoggingEntity sle = sessionLogginRepository.Query().
                FirstOrDefault(x => x.GLKNumber == friendGLKNumber && x.LastConnection >= toCheck && x.Unlogged != true);

            if (sle != null)
                return (bool)sle.isOnline;
            else
                return false;
        
        }

        public string GetFriendIP(int friendGLKNumber)
        {
            var toCheck = DateTime.Now.AddMinutes(-1);
            SessionLoggingEntity sle = sessionLogginRepository.Query().FirstOrDefault(x => x.GLKNumber == friendGLKNumber && x.LastConnection >= toCheck);

            if (sle != null)
                return sle.UserIP;
            else
                return "";

        }

        public Accounts GetFriendsList(int userGLKNumber)
        {
            IEnumerable<UserFriendEntity> userFriends = userFriendRepository.Query().Where(x => x.OwnerGLKNumber == userGLKNumber && x.del == false);

            var friendsList = Mapper.Map<IEnumerable<UserFriendEntity>, IEnumerable<Account>>(userFriends);
            Accounts toReturn = new Accounts { };

            foreach (var friend in friendsList)
            {
                var temp = accountService.getUserEntity(friend.GLKNumber);
                friend.isOnline = GetFriendStatus(friend.GLKNumber);
                friend.Firstname = temp.Firstname;
                friend.Surname = temp.Surname;
                friend.Email = temp.Email;
                friend.BirthDay = temp.BirthDay.ToString();
                friend.UserIP = GetFriendIP(friend.GLKNumber);
                

                toReturn.Add(friend);
            }

            return toReturn;
        }

    }
}
