﻿using AutoMapper;
using BusinessLogic.Contract;
using BusinessLogic.Contracts;
using DataAccessLayer.Contracts;
using DataAccessLayer.Implementation.Entites;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Implementation
{
    public class MessageService : IMessageService
    {
        public IAccountService accountService { get; set; }
        public IRepository<MessageEntity> messageRepository { get; set; }

        public IUnitOfWork unitOfWork { get; set; }

        public MessageService(IRepository<MessageEntity> _message, IUnitOfWork _unit, IAccountService _account)
        {
            accountService = _account;
            messageRepository = _message;
            unitOfWork = _unit;
        }

        public Message SendMessage(Message message)
        {
            MessageEntity mToAdd = new MessageEntity
            {
                Message = message.MessageContent,
                ReceiverGLKNumber = message.ReceiverGLKNumber,
                SenderGLKNumber = message.SenderGLKNumber,
                SendDate = DateTime.Now
            };

            messageRepository.Add(mToAdd);
            unitOfWork.Commit();

            message.SendDate = DateTime.Now;


            return message;

        }

        public Messages GetMessages(string SessionToken, int UserGLKNumber)
        {
            Messages messagesToReturn = new Messages { };

            IEnumerable<MessageEntity> messageEntites = messageRepository.Query().Where(x => x.ReceiverGLKNumber == UserGLKNumber && x.Sent == false).
                OrderByDescending(x => x.SendDate);

            IEnumerable<Message> messageToReturn = Mapper.Map<IEnumerable<MessageEntity>, IEnumerable<Message>>(messageEntites);
            Messages messages = new Messages { };

            foreach (var m in messageToReturn)
            {
                messages.Add(m);
            }

            foreach (var mEntity in messageEntites)
            {
                mEntity.Sent = true;
            }

            unitOfWork.Commit();

            return messages;
        }



        public Messages GetMessagesFromUser(string SessionToken, int UserGLKNumber, int FriendGLKNumber)
        {
            Messages messagesToReturn = new Messages { };

            IEnumerable<MessageEntity> messageEntites = messageRepository.Query().
                Where(x => x.ReceiverGLKNumber == UserGLKNumber && x.Sent == false && x.SenderGLKNumber == FriendGLKNumber).
                OrderByDescending(x => x.SendDate);

            IEnumerable<Message> messageToReturn = Mapper.Map<IEnumerable<MessageEntity>, IEnumerable<Message>>(messageEntites);
            Messages messages = new Messages { };

            foreach (var m in messageToReturn)
            {
                messages.Add(m);
            }

            foreach (var mEntity in messageEntites)
            {
                mEntity.Sent = true;
            }

            unitOfWork.Commit();

            return messages;
        }
    }
}
